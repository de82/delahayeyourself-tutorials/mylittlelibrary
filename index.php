<?php require "vendor/autoload.php";

use \Tamtamchik\SimpleFlash\Flash;
use \Respect\Validation\Exceptions\NestedValidationException;

session_start();

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function($string){
        return renderHTMLFromMarkdown($string);
    }));
    $twig->addFilter(new Twig_Filter('date_format', function($string){
        if($string){
            $string = str_replace("/", '-', $string);
            $dates = explode("-", $string);
            return sprintf("%s-%s-%s", $dates[0], $dates[1], $dates[2]);
        }
        return $string;
    }));
    $twig->addFunction(new Twig_Function('messages', function(){
        return Flash::display();
    }));
});



Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:ingen.sqlite3');
});


Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});


Flight::route('/', function(){
    echo 'Hello World!';
});

Flight::route('/hello/@name', function($name){
    echo "Hello $name!";
});


Flight::route('/hello/@name/@firstname', function($name, $firstname){
    $data = [
        'name' => $name,
        'firstname' => $firstname,
    ];

    Flight::render('hello.twig', $data);
});


Flight::route('/first_view/', function(){
    $data = [
        'contenu' => 'Lorem ipsum',
    ];
    Flight::render('first_view.twig', $data);
});


Flight::route('/books/', function(){
    $data = [
        'books' => getBooks(),
    ];
    Flight::render('books.twig', $data);
});


Flight::route('/page/', function(){
    $data = [
        'page_content' => getPageContent("test"),
    ];
    Flight::render('page.twig', $data);
});


Flight::route('/flash', function(){
    Flash::warning('Hello there');
    Flash::info('Hello there');
    Flash::error('Hello there');
    Flash::success('Hello there');

    Flight::render('flash.twig');
});


Flight::route('/dinosaurs/', function(){
    $data = [
        'dinosaurs' => Dinosaur::find_many(),
    ];
    Flight::render('dinosaurs/list.twig', $data);
});


Flight::route('/dinosaurs/form(/@id)', function($id){
    //1. Create a empty dinosaur
    if($id){
        $dinosaur = Model::factory('Dinosaur')->find_one($id);
    }else{
        $dinosaur = Model::factory('Dinosaur')->create();
        $dinosaur->name = null;
        $dinosaur->model_id = null;
        $dinosaur->avatar = null;
        $dinosaur->birthday = null;
    }
    

    //2. if method is POST
    if(Flight::request()->method == 'POST'){

        if(Flight::request()->files['avatar'] && Flight::request()->files['avatar']['tmp_name']){
            $avatar_file = convertFileToBlob(Flight::request()->files['avatar']['tmp_name']);
            $dinosaur->avatar = $avatar_file;
        }
        

        $dinosaur->name = Flight::request()->data->name;
        $dinosaur->model_id = Flight::request()->data->model_id;
        $dinosaur->birthday = Flight::request()->data->birthday;
        

        if(validateDinosaur($dinosaur)){
            if($id){
                Flash::success(sprintf('Dinosaur %s edited!', $dinosaur->name));
            }else{
                Flash::success(sprintf('Dinosaur %s added!', $dinosaur->name));
            }
            $dinosaur->save();
            Flight::redirect('/dinosaurs');
        }
        else{
            Flash::warning('Ooops some errors are in your form!');
            $data = [
                'dinosaur' => $dinosaur,
                'models' => IngenModel::find_many(),
            ];
            Flight::render('dinosaurs/form.twig', $data);
        }
        
    }else{
        $data = [
            'dinosaur' => $dinosaur,
            'models' => IngenModel::find_many(),
        ];
        Flight::render('dinosaurs/form.twig', $data);
    }
});


Flight::route('/dinosaurs/delete/@id', function($id){
    $dinosaur = Model::factory('Dinosaur')->find_one($id);
    Flash::success(sprintf('Dinosaur %s deleted!', $dinosaur->name));
    $dinosaur->delete();
    Flight::redirect('/dinosaurs');
});

Flight::route('/dinosaurs/@name', function($name){
    $data = [
        'dino' => Model::factory('Dinosaur')
                        ->where('name', $name)
                        ->find_one(),
    ];
    Flight::render('dinosaurs/detail.twig', $data);
});


Flight::start();
