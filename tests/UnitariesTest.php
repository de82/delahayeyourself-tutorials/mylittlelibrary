<?php require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase {

    public function test_multiply(){
        $this->assertEquals(4, multiply(2, 2));
        $this->assertEquals(21, multiply(3, 7));
    }

    public function test_getBoooks(){
        $books = getBooks();
        $this->assertIsArray($books);
        $this->assertEquals(5, count($books));
        $this->assertIsString($books[0]->title);
    }

    public function test_renderHTMLFromMarkdown()
    {
        $this->assertEquals("<h2>Test</h2>", renderHTMLFromMarkdown("## Test"));
    }


    public function test_readFileContent()
    {
        $this->assertEquals("## Test", readFileContent("pages/test.md"));

        $this->expectException(Exception::class);
        readFileContent("another_file");
    }

    public function test_getPageContent()
    {
        $this->assertEquals("## Test", getPageContent("test"));
    }
}
