<?php require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("Hello World!", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_hello_name()
    {
        $name = 'Ben';
        $response = $this->make_request("GET", "/hello/$name");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("Hello $name!", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_hello_firstname_name()
    {
        $name = 'Kenobi';
        $firstname = 'Ben';

        $response = $this->make_request("GET", "/hello/$name/$firstname");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Hello $firstname $name!", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }


    public function test_first_view()
    {
        $response = $this->make_request("GET", "/first_view/");

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString("Hello Twig First View!", $response->getBody()->getContents());
        $this->assertStringContainsString("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_books(){
        $response = $this->make_request("GET", "/books/");
        $books = getBooks();
        $this->assertEquals(200, $response->getStatusCode());
        $body = $response->getBody()->getContents();
        foreach($books as $book){
            $this->assertStringContainsString($book->title, $body);
            $this->assertStringContainsString($book->cover, $body);
        }
    }

    public function test_pages()
    {
        $response = $this->make_request("GET", "/page/");
        $this->assertEquals(200, $response->getStatusCode());
        $body = $response->getBody()->getContents();
        $html_from_test_page_file = renderHTMLFromMarkdown(readFileContent("pages/test.md"));
        $this->assertStringContainsString($html_from_test_page_file, $body);
    }
}