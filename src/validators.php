<?php

use \Tamtamchik\SimpleFlash\Flash;
use Respect\Validation\Validator as v;


function isDate($value) 
{
    if (!$value) {
        return false;
    }

    try {
        new \DateTime($value);
        return true;
    } catch (\Exception $e) {
        return false;
    }
}


function validateDinosaur($dinosaur){
    if(is_null($dinosaur->name) or strlen($dinosaur->name) < 3){
        Flash::warning(sprintf('Dinosaur name "%s" is not acceptable.', $dinosaur->name));
        return false;
    }
    if(!isDate($dinosaur->birthday)){
        Flash::warning(sprintf('Dinosaur birthday "%s" is not acceptable.', $dinosaur->birthday));
        return false;
    }

    return true;
}