<?php

class Dinosaur extends Model {
    public static $_table = 'Dinosaurs';

    public function model()
    {
        return $this->belongs_to('IngenModel', 'model_id')->find_one();
    }

    public function avatar_b64()
    {
        if($this->avatar){
            return sprintf("data:image/jpeg;base64,%s", base64_encode($this->avatar));
        }else{
            return null;
        }
        
    }
}

class IngenModel extends Model {
    public static $_table = 'Models';

    public function specie()
    {
        return $this->belongs_to('Specie', 'specie_id')->find_one();
    }
}


class Specie extends Model {
    public static $_table = 'Species';
}
