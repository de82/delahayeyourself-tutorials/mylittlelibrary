<?php

use Michelf\Markdown;


function multiply($facteur_gauche, $facteur_droite){
    return $facteur_gauche * $facteur_droite;
}

function getBooks(){
    $response = Requests::get('https://medusa.delahayeyourself.info/api/books/');
    return json_decode($response->body);
}

function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return trim(Markdown::defaultTransform($string_markdown_formatted));
}

function readFileContent($filepath){
    if(file_exists($filepath)){
        return file_get_contents($filepath);
    }
    throw new Exception("File doesn't exist!");
}


function getPageContent($page_name)
{
    $filepath = sprintf("pages/%s.md", $page_name);
    return readFileContent($filepath);
}


function convertFileToBlob($file)
{
    return file_get_contents($file);
}