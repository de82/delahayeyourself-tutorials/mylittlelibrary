# 📣 Disclaimer

> Le repository git que vous êtes en train de visualiser n'a eu qu'une vocation pédagogique pour accompagner les modules enseignés en DUT 1A, 2A, LPRGI et 1A de cycle ingénieur.

> Son propos n'avait qu'une portée pédagogique et peut ne pas refléter l'état actuel ou encore les bonnes pratiques de la/les technologie(s) utilisée. 

## 🎯 Utilisation

Vous êtes libre de réutiliser librement le code présent dans ce repository. Prenez garde que le code est ici daté, non mis à jour et potentiellement ouvert aux failles et/ou bugs.

## 🦕 Crédit

[Samy Delahaye](https://delahayeyourself.info)


## 🪴 Descriptif du projet


### ![](assets/img/logo.png)

Un simple site web pour découvrir le pattern MVC avec une approche TDD via l'utilisation de:

- [Composer](https://getcomposer.org/),
- [FlightPHP](http://flightphp.com/), 
- [Twig](https://twig.symfony.com/),
- [Requests](http://requests.ryanmccue.info/),
- [PHPUnit](https://phpunit.de/),
- et bien d'autres choses ..

## How to ?

1. `composer install`
2. `vendor/bin/phpunit tests/`
3. 127.0.0.1:5000
4. Et go sur [127.0.0.1:5000](http://127.0.0.1:5000)

## Tests ?

La couverture est minimal mais suffisante pour découvrir les T.U et d'intégrations avec PHPUnit. 

Pour les lancer un simple: `vendor/bin/phpunit tests/`

## Requirements

```
mikecao/flight
symfony/process
guzzlehttp/guzzle
phpunit/phpunit
twig/twig
rmccue/requests
michelf/php-markdown
```

## Purpose

This project is only for educational purpose. It's actually a basic application for generating a dummy website.

## Authors

> [Samy Delahaye](https://delahayeyourself.info)
